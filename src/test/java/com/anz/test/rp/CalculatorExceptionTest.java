package com.anz.test.rp;

import static org.junit.Assert.*;
import junit.framework.Assert;

import org.junit.Test;

public class CalculatorExceptionTest {

	@Test
	public final void testCalculatorException() {
		Throwable t = new CalculatorException();
		Assert.assertNotNull(t);
	}

	@Test
	public final void testCalculatorExceptionString() {
		Throwable t = new CalculatorException("Test");
		Assert.assertNotNull(t);
	}

	@Test
	public final void testCalculatorExceptionThrowable() {
		Throwable t = new CalculatorException(new Exception ("Test"));
		Assert.assertNotNull(t);
		
		Assert.assertEquals(t.getCause().getMessage(), "Test");
	}

	@Test
	public final void testCalculatorExceptionStringThrowable() {
		Throwable t = new CalculatorException("Test", new Exception ("Test"));
		Assert.assertNotNull(t);
		
		Assert.assertEquals(t.getMessage(), "Test");
		Assert.assertEquals(t.getCause().getMessage(), "Test");
	}

}
