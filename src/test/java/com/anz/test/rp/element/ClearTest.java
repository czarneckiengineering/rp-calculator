package com.anz.test.rp.element;

import java.math.BigDecimal;
import java.util.Iterator;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.anz.test.rp.CalculatorException;
import com.anz.test.rp.manager.StackManager;

@RunWith(MockitoJUnitRunner.class)
public class ClearTest {
	
	@Mock private StackManager stackManager;

	@Test
	public final void testUndo() throws CalculatorException {
		Mockito.when(stackManager.pop()).thenReturn(new NumericOperand(new BigDecimal(2))).thenReturn(new NumericOperand(new BigDecimal(4)));

		Clear clear = new Clear();
		
		clear.execute(stackManager);
		
		Iterator<Operator> itr = clear.undo();
		
		Assert.assertFalse(itr.hasNext());
	}

	@Test
	public final void testExecute() throws CalculatorException {
		Mockito.doAnswer (new Answer<Object>() {

			public Object answer(InvocationOnMock invocation) throws Throwable {
				return null;
			}
		}).when(stackManager).clear();
		
		Clear clear = new Clear();
		
		clear.execute(stackManager);

		Mockito.verify(stackManager).clear();
	}

	@Test
	public final void testExecuteNullHandler() throws CalculatorException {
		Mockito.doAnswer (new Answer<Object>() {

			public Object answer(InvocationOnMock invocation) throws Throwable {
				return null;
			}
		}).when(stackManager).clear();
		
		Clear clear = new Clear();
		
		clear.execute(null);
	}

	@Test
	public final void testCreate() {
		Clear clear = new Clear();
		
		Element element = clear.create(Clear.CLEAR);
		
		Assert.assertTrue(element instanceof Clear);
	}

	@Test
	public final void testCreateInvalidToken() {
		Clear clear = new Clear();
		
		Element element = clear.create(Divide.DIVIDE);
		
		Assert.assertNull(element);
	}

}
