package com.anz.test.rp.element;

import java.math.BigDecimal;
import java.util.Iterator;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.anz.test.rp.CalculatorException;
import com.anz.test.rp.manager.StackManager;

@RunWith(MockitoJUnitRunner.class)
public class PushTest {
	
	@Mock private StackManager stackManager;

	@Test
	public final void testUndo() throws CalculatorException {
		Push push = new Push(new NumericOperand(new BigDecimal(2)));
		
		push.execute(stackManager);
		
		Iterator<Operator> itr = push.undo();
		
		Assert.assertTrue(itr.hasNext());
		
		Assert.assertTrue(itr.next() instanceof Pop);
		
	}

	@Test
	public final void testExecute() throws CalculatorException {
		Mockito.doAnswer (new Answer<Object>() {

			public Object answer(InvocationOnMock invocation) throws Throwable {
				Assert.assertNotNull(invocation.getArguments()[0]);
				
				Assert.assertTrue(invocation.getArguments()[0] instanceof NumericOperand);
				
				NumericOperand operand = (NumericOperand)invocation.getArguments()[0];
				
				Assert.assertEquals(operand.getOperand().intValue(), 2);
				
				return null;
			}
		}).when(stackManager).push(Mockito.<Operand>any());
		
		Push push = new Push(new NumericOperand(new BigDecimal(2)));
		
		push.execute(stackManager);
	}

	@Test
	public final void testExecuteNullHandler() throws CalculatorException {
		Mockito.doAnswer (new Answer<Object>() {

			public Object answer(InvocationOnMock invocation) throws Throwable {
				Assert.assertNotNull(invocation.getArguments()[0]);
				
				Assert.assertTrue(invocation.getArguments()[0] instanceof NumericOperand);
				
				NumericOperand operand = (NumericOperand)invocation.getArguments()[0];
				
				Assert.assertEquals(operand.getOperand().intValue(), 2);
				
				return null;
			}
		}).when(stackManager).push(Mockito.<Operand>any());
		
		Push push = new Push(new NumericOperand(new BigDecimal(2)));
		
		push.execute(null);
	}

	@Test
	public final void testCreate() {
		Push push = new Push(new NumericOperand(new BigDecimal(2)));
		
		Element element = push.create("12.34");
		
		Assert.assertNull(element);
	}

}
