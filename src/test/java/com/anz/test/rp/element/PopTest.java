package com.anz.test.rp.element;

import java.math.BigDecimal;
import java.util.Iterator;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.anz.test.rp.CalculatorException;
import com.anz.test.rp.manager.StackManager;

@RunWith(MockitoJUnitRunner.class)
public class PopTest {
	
	@Mock private StackManager stackManager;

	@Test
	public final void testUndo() throws CalculatorException {
		Mockito.when(stackManager.pop()).thenReturn(new NumericOperand(new BigDecimal(2)));

		Pop pop = new Pop();
		
		pop.execute(stackManager);
		
		Iterator<Operator> itr = pop.undo();
		
		Assert.assertTrue(itr.hasNext());
		
		Assert.assertTrue(itr.next() instanceof Push);
		
	}

	@Test
	public final void testExecute() throws CalculatorException {
		Mockito.when(stackManager.pop()).thenReturn(new NumericOperand(new BigDecimal(2)));
		
		Pop pop = new Pop();
		
		pop.execute(stackManager);
		
		Assert.assertEquals(pop.getOperand().getOperand().intValue(), 2);

	}

	@Test
	public final void testExecuteNullHandler() throws CalculatorException {
		Mockito.when(stackManager.pop()).thenReturn(new NumericOperand(new BigDecimal(2)));
		
		Pop pop = new Pop();
		
		pop.execute(null);
	}

	@Test
	public final void testCreate() {
		Pop pop = new Pop();
		
		Element element = pop.create("12.34");
		
		Assert.assertNull(element);
	}

}
