package com.anz.test.rp.element;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class NumericOperandTest {
	
	@Test
	public final void testGetOperand() {
		NumericOperand numericOperand = new NumericOperand();
		
		Element element = numericOperand.create("12.34");
		
		Assert.assertTrue(element instanceof NumericOperand);
		
		Assert.assertEquals("12.34", ((NumericOperand)element).getOperand().toPlainString());
		
	}

	@Test
	public final void testCreate() {
		NumericOperand numericOperand = new NumericOperand();
		
		Element element = numericOperand.create("12.34");
		
		Assert.assertTrue(element instanceof NumericOperand);
	}

	@Test
	public final void testCreateInvalidNumber() {
		NumericOperand numericOperand = new NumericOperand();
		
		Element element = numericOperand.create("twenty");
		
		Assert.assertNull(element);
	}

}
