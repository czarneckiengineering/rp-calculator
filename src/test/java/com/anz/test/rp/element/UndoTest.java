package com.anz.test.rp.element;

import java.util.Iterator;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.anz.test.rp.CalculatorException;
import com.anz.test.rp.manager.CommandManager;

@RunWith(MockitoJUnitRunner.class)
public class UndoTest {
	
	@Mock private CommandManager commandManager;

	@Test
	public final void testUndo() throws CalculatorException {
		Undo undo = new Undo();
		
		undo.execute(commandManager);
		
		Iterator<Operator> itr = undo.undo();
		
		Assert.assertFalse(itr.hasNext());
	}

	@Test
	public final void testExecute() throws CalculatorException {
		Mockito.doAnswer (new Answer<Object>() {

			public Object answer(InvocationOnMock invocation) throws Throwable {
				return null;
			}
		}).when(commandManager).undo();
		
		Undo undo = new Undo();
		
		undo.execute(commandManager);

		Mockito.verify(commandManager).undo();
	}

	@Test
	public final void testExecuteNullHandler() throws CalculatorException {
		Mockito.doAnswer (new Answer<Object>() {

			public Object answer(InvocationOnMock invocation) throws Throwable {
				return null;
			}
		}).when(commandManager).undo();
		
		Undo undo = new Undo();
		
		undo.execute(null);
	}

	@Test
	public final void testCreate() {
		Undo undo = new Undo();
		
		Element element = undo.create(Undo.UNDO);
		
		Assert.assertTrue(element instanceof Undo);
	}

	@Test
	public final void testCreateInvalidToken() {
		Undo undo = new Undo();
		
		Element element = undo.create(Add.ADD);
		
		Assert.assertNull(element);
	}

}
