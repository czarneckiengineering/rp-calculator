package com.anz.test.rp.element;

import java.math.BigDecimal;
import java.util.Iterator;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.anz.test.rp.CalculatorException;
import com.anz.test.rp.manager.StackManager;

@RunWith(MockitoJUnitRunner.class)
public class DivideTest {
	
	@Mock private StackManager stackManager;

	@Test
	public final void testUndo() throws CalculatorException {
		Mockito.when(stackManager.pop()).thenReturn(new NumericOperand(new BigDecimal(4))).thenReturn(new NumericOperand(new BigDecimal(2)));

		Divide divide = new Divide();
		
		divide.execute(stackManager);
		
		Iterator<Operator> itr = divide.undo();
		
		Assert.assertTrue(itr.hasNext());
		
		Assert.assertTrue(itr.next() instanceof Pop);
		Assert.assertTrue(itr.next() instanceof Push);
		Assert.assertTrue(itr.next() instanceof Push);
		
	}

	@Test
	public final void testExecute() throws CalculatorException {
		Mockito.when(stackManager.pop()).thenReturn(new NumericOperand(new BigDecimal(2))).thenReturn(new NumericOperand(new BigDecimal(4)));
		
		Mockito.doAnswer (new Answer<Object>() {

			public Object answer(InvocationOnMock invocation) throws Throwable {
				Assert.assertNotNull(invocation.getArguments()[0]);
				
				Assert.assertTrue(invocation.getArguments()[0] instanceof NumericOperand);
				
				NumericOperand operand = (NumericOperand)invocation.getArguments()[0];
				
				Assert.assertEquals(operand.getOperand().intValue(), 2);
				
				return null;
			}
		}).when(stackManager).push(Mockito.<Operand>any());
		
		Divide divide = new Divide();
		
		divide.execute(stackManager);
	}

	@Test
	public final void testExecuteNullHandler() throws CalculatorException {
		Mockito.when(stackManager.pop()).thenReturn(new NumericOperand(new BigDecimal(2))).thenReturn(new NumericOperand(new BigDecimal(4)));
		
		Mockito.doAnswer (new Answer<Object>() {

			public Object answer(InvocationOnMock invocation) throws Throwable {
				Assert.assertNotNull(invocation.getArguments()[0]);
				
				Assert.assertTrue(invocation.getArguments()[0] instanceof NumericOperand);
				
				NumericOperand operand = (NumericOperand)invocation.getArguments()[0];
				
				Assert.assertEquals(operand.getOperand().intValue(), 2);
				
				return null;
			}
		}).when(stackManager).push(Mockito.<Operand>any());
		
		Divide divide = new Divide();
		
		divide.execute(null);
	}

	@Test (expected = CalculatorException.class)
	public final void testExecuteDivideByZero() throws CalculatorException {
		Mockito.when(stackManager.pop()).thenReturn(new NumericOperand(new BigDecimal(0))).thenReturn(new NumericOperand(new BigDecimal(4)));
		
		Mockito.doAnswer (new Answer<Object>() {

			public Object answer(InvocationOnMock invocation) throws Throwable {
				throw new ArithmeticException();
			}
		}).when(stackManager).push(Mockito.<Operand>any());
		
		Divide divide = new Divide();
		
		divide.execute(stackManager);
	}


	@Test (expected = CalculatorException.class)
	public final void testExecuteInfinitePrecision() throws CalculatorException {
		Mockito.when(stackManager.pop()).thenReturn(new NumericOperand(new BigDecimal(3))).thenReturn(new NumericOperand(new BigDecimal(1)));
		
		Mockito.doAnswer (new Answer<Object>() {

			public Object answer(InvocationOnMock invocation) throws Throwable {
				throw new ArithmeticException();
			}
		}).when(stackManager).push(Mockito.<Operand>any());
		
		Divide divide = new Divide();
		
		divide.execute(stackManager);
	}

	@Test
	public final void testCreate() {
		Divide divide = new Divide();
		
		Element element = divide.create(Divide.DIVIDE);
		
		Assert.assertTrue(element instanceof Divide);
	}

	@Test
	public final void testCreateInvalidToken() {
		Divide divide = new Divide();
		
		Element element = divide.create(Add.ADD);
		
		Assert.assertNull(element);
	}

}
