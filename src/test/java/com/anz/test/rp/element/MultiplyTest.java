package com.anz.test.rp.element;

import java.math.BigDecimal;
import java.util.Iterator;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.anz.test.rp.CalculatorException;
import com.anz.test.rp.manager.StackManager;

@RunWith(MockitoJUnitRunner.class)
public class MultiplyTest {
	
	@Mock private StackManager stackManager;

	@Test
	public final void testUndo() throws CalculatorException {
		Mockito.when(stackManager.pop()).thenReturn(new NumericOperand(new BigDecimal(2))).thenReturn(new NumericOperand(new BigDecimal(4)));

		Multiply multiply = new Multiply();
		
		multiply.execute(stackManager);
		
		Iterator<Operator> itr = multiply.undo();
		
		Assert.assertTrue(itr.hasNext());
		
		Assert.assertTrue(itr.next() instanceof Pop);
		Assert.assertTrue(itr.next() instanceof Push);
		Assert.assertTrue(itr.next() instanceof Push);
		
	}

	@Test
	public final void testExecute() throws CalculatorException {
		Mockito.when(stackManager.pop()).thenReturn(new NumericOperand(new BigDecimal(2))).thenReturn(new NumericOperand(new BigDecimal(4)));
		
		Mockito.doAnswer (new Answer<Object>() {

			public Object answer(InvocationOnMock invocation) throws Throwable {
				Assert.assertNotNull(invocation.getArguments()[0]);
				
				Assert.assertTrue(invocation.getArguments()[0] instanceof NumericOperand);
				
				NumericOperand operand = (NumericOperand)invocation.getArguments()[0];
				
				Assert.assertEquals(operand.getOperand().intValue(), 8);
				
				return null;
			}
		}).when(stackManager).push(Mockito.<Operand>any());
		
		Multiply multiply = new Multiply();
		
		multiply.execute(stackManager);
	}

	@Test
	public final void testExecuteNullHandler() throws CalculatorException {
		Mockito.when(stackManager.pop()).thenReturn(new NumericOperand(new BigDecimal(2))).thenReturn(new NumericOperand(new BigDecimal(4)));
		
		Mockito.doAnswer (new Answer<Object>() {

			public Object answer(InvocationOnMock invocation) throws Throwable {
				Assert.assertNotNull(invocation.getArguments()[0]);
				
				Assert.assertTrue(invocation.getArguments()[0] instanceof NumericOperand);
				
				NumericOperand operand = (NumericOperand)invocation.getArguments()[0];
				
				Assert.assertEquals(operand.getOperand().intValue(), 8);
				
				return null;
			}
		}).when(stackManager).push(Mockito.<Operand>any());
		
		Multiply multiply = new Multiply();
		
		multiply.execute(null);
	}

	@Test
	public final void testCreate() {
		Multiply multiply = new Multiply();
		
		Element element = multiply.create(Multiply.MULTIPLY);
		
		Assert.assertTrue(element instanceof Multiply);
	}

	@Test
	public final void testCreateInvalidToken() {
		Multiply multiply = new Multiply();
		
		Element element = multiply.create(Divide.DIVIDE);
		
		Assert.assertNull(element);
	}

}
