package com.anz.test.rp.element;

import java.math.BigDecimal;
import java.util.Iterator;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.anz.test.rp.CalculatorException;
import com.anz.test.rp.manager.StackManager;

@RunWith(MockitoJUnitRunner.class)
public class SubtractTest {
	
	@Mock private StackManager stackManager;

	@Test
	public final void testUndo() throws CalculatorException {
		Mockito.when(stackManager.pop()).thenReturn(new NumericOperand(new BigDecimal(2))).thenReturn(new NumericOperand(new BigDecimal(5)));

		Subtract subtract = new Subtract();
		
		subtract.execute(stackManager);
		
		Iterator<Operator> itr = subtract.undo();
		
		Assert.assertTrue(itr.hasNext());
		
		Assert.assertTrue(itr.next() instanceof Pop);
		Assert.assertTrue(itr.next() instanceof Push);
		Assert.assertTrue(itr.next() instanceof Push);
		
	}

	@Test
	public final void testExecute() throws CalculatorException {
		Mockito.when(stackManager.pop()).thenReturn(new NumericOperand(new BigDecimal(2))).thenReturn(new NumericOperand(new BigDecimal(5)));
		
		Mockito.doAnswer (new Answer<Object>() {

			public Object answer(InvocationOnMock invocation) throws Throwable {
				Assert.assertNotNull(invocation.getArguments()[0]);
				
				Assert.assertTrue(invocation.getArguments()[0] instanceof NumericOperand);
				
				NumericOperand operand = (NumericOperand)invocation.getArguments()[0];
				
				Assert.assertEquals(operand.getOperand().intValue(), 3);
				
				return null;
			}
		}).when(stackManager).push(Mockito.<Operand>any());
		
		Subtract subtract = new Subtract();
		
		subtract.execute(stackManager);
	}

	@Test
	public final void testExecuteNullHandler() throws CalculatorException {
		Mockito.when(stackManager.pop()).thenReturn(new NumericOperand(new BigDecimal(2))).thenReturn(new NumericOperand(new BigDecimal(5)));
		
		Mockito.doAnswer (new Answer<Object>() {

			public Object answer(InvocationOnMock invocation) throws Throwable {
				Assert.assertNotNull(invocation.getArguments()[0]);
				
				Assert.assertTrue(invocation.getArguments()[0] instanceof NumericOperand);
				
				NumericOperand operand = (NumericOperand)invocation.getArguments()[0];
				
				Assert.assertEquals(operand.getOperand().intValue(), 3);
				
				return null;
			}
		}).when(stackManager).push(Mockito.<Operand>any());
		
		Subtract subtract = new Subtract();
		
		subtract.execute(null);
	}

	@Test
	public final void testCreate() {
		Subtract subtract = new Subtract();
		
		Element element = subtract.create(Subtract.SUBTRACT);
		
		Assert.assertTrue(element instanceof Subtract);
	}

	@Test
	public final void testCreateInvalidToken() {
		Subtract subtract = new Subtract();
		
		Element element = subtract.create(Add.ADD);
		
		Assert.assertNull(element);
	}

}
