package com.anz.test.rp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.anz.test.rp.manager.CommandManager;
import com.anz.test.rp.manager.ConsoleManager;
import com.anz.test.rp.manager.StackManager;


@RunWith(MockitoJUnitRunner.class)
public class CalculatorTest {
	
	@InjectMocks private Calculator calculator = new Calculator();
	
	@Mock ConsoleManager consoleManager;
	
	@Mock StackManager stackManager;
	
	@Mock CommandManager commandManager;

	@Test
	public final void testHandle() throws CalculatorException {
		Mockito.doAnswer (new Answer<Object>() {

			public Object answer(InvocationOnMock invocation) throws Throwable {
				return null;
			}
		}).when(consoleManager).handle();
		
		calculator.handle();
	}

	@Test
	public final void testHandleCalculatorException() throws CalculatorException {
		Mockito.doAnswer (new Answer<Object>() {

			public Object answer(InvocationOnMock invocation) throws Throwable {
				throw new CalculatorException();
			}
		}).when(consoleManager).handle();
		
		calculator.handle();
	}

}
