package com.anz.test.rp;

import java.util.MissingResourceException;

import org.junit.Assert;
import org.junit.Test;

public class MessagesTest {

	@Test
	public final void testGetString() {
		Assert.assertEquals(Messages.getString("Calculator.1"), "Exiting...");
	}

	@Test
	public final void testGetStringInvalidKey() {
		Assert.assertEquals(Messages.getString("Calculator.100"), "!Calculator.100!");
	}

}
