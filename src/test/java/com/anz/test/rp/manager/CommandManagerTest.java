package com.anz.test.rp.manager;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.anz.test.rp.CalculatorException;
import com.anz.test.rp.element.Add;

@RunWith(MockitoJUnitRunner.class)
public class CommandManagerTest {
	
	@InjectMocks CommandManager commandManager = new CommandManager();
	
	@Mock ConsoleManager consoleManager;
	
	@Mock StackManager stackManager;

	@Test
	public final void testHandle() throws CalculatorException {
		commandManager.handle(new Add());
	}

	@Test
	public final void testParse() {
	}

	@Test
	public final void testSetConsoleManager() {
		commandManager.setConsoleManager(consoleManager);
	}

	@Test
	public final void testSetStackManager() {
		commandManager.setStackManager(stackManager);
	}

	@Test
	public final void testAddCommand() {
		commandManager.addCommand(new Add());
	}

	@Test
	public final void testUndo() {
	}

}
