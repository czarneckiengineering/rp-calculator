package com.anz.test.rp.manager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.anz.test.rp.CalculatorException;

@RunWith(MockitoJUnitRunner.class)
public class ConsoleManagerTest {
	
	@InjectMocks ConsoleManager consoleManager = new ConsoleManager();
	
	@Mock CommandManager commandManager;
	
	@Mock StackManager stackManager;
	
	@Mock PrintStream out;
	
	@Mock BufferedReader reader;

	@Test
	public final void testSetCommandManager() {
		consoleManager.setCommandManager(commandManager);
	}

	@Test
	public final void testSetStackManager() {
		consoleManager.setStackManager(stackManager);
	}

	@Test
	public final void testSetReader() {
		consoleManager.setReader(reader);
	}

	@Test
	public final void testSetOut() {
		consoleManager.setOut(out);
	}

	@Test
	public final void testHandle() throws IOException, CalculatorException {
		Mockito.when(reader.readLine()).thenReturn("1 2 +").thenReturn(null);
		
		Mockito.doAnswer (new Answer<Object>() {

			public Object answer(InvocationOnMock invocation) throws Throwable {
				Assert.assertNotNull(invocation.getArguments()[0]);
				
				Assert.assertTrue(invocation.getArguments()[0] instanceof String);
				
				String token = (String)invocation.getArguments()[0];
				
				Assert.assertTrue("1".equals(token) || "2".equals(token) || "+".equals(token));
				
				return null;
			}
		}).when(commandManager).parse(Mockito.<String>any());
		
		consoleManager.handle();
	}

	@Test
	public final void testWrite() {
		consoleManager.write("Test");
		Mockito.verify(out).println(Mockito.<String>any());
	}

}
