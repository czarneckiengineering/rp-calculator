package com.anz.test.rp.manager;

import java.math.BigDecimal;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.anz.test.rp.CalculatorException;
import com.anz.test.rp.element.Clear;
import com.anz.test.rp.element.NumericOperand;
import com.anz.test.rp.element.Operand;
import com.anz.test.rp.element.Push;

@RunWith(MockitoJUnitRunner.class)
public class StackManagerTest {
	
	@Mock ConsoleManager consoleManager;
	
	@InjectMocks private StackManager stackManager = new StackManager();

	@Test
	public final void testSetConsoleManager() throws CalculatorException {
		Mockito.doAnswer (new Answer<Object>() {

			public Object answer(InvocationOnMock invocation) throws Throwable {
				Assert.assertNotNull(invocation.getArguments()[0]);
				
				Assert.assertTrue(invocation.getArguments()[0] instanceof String);
				
				String operand = (String)invocation.getArguments()[0];
				
				Assert.assertEquals(operand, "stack: ");
				
				return null;
			}
		}).when(consoleManager).write(Mockito.<String>any());
		
		stackManager.setConsoleManager(consoleManager);
		
		stackManager.write();
	}

	@Test
	public final void testHandle() throws CalculatorException {
		stackManager.handle(new Push(new NumericOperand(new BigDecimal("12.34"))));
		
		NumericOperand numericOperand = (NumericOperand) stackManager.pop();
		
		Assert.assertEquals(numericOperand.getOperand().toPlainString(), "12.34");
	}

	@Test
	public final void testPush() throws CalculatorException {
		stackManager.push(new NumericOperand(new BigDecimal("12.34")));
		
		NumericOperand numericOperand = (NumericOperand) stackManager.pop();
		
		Assert.assertEquals(numericOperand.getOperand().toPlainString(), "12.34");
	}

	@Test
	public final void testPop() throws CalculatorException {
		stackManager.push(new NumericOperand(new BigDecimal("12.34")));
		
		NumericOperand numericOperand = (NumericOperand) stackManager.pop();
		
		Assert.assertEquals(numericOperand.getOperand().toPlainString(), "12.34");
	}

	@Test
	public final void testWrite() throws CalculatorException {
		stackManager.push(new NumericOperand(new BigDecimal (1)));
		
		Mockito.doAnswer (new Answer<Object>() {

			public Object answer(InvocationOnMock invocation) throws Throwable {
				Assert.assertNotNull(invocation.getArguments()[0]);
				
				Assert.assertTrue(invocation.getArguments()[0] instanceof String);
				
				String operand = (String)invocation.getArguments()[0];
				
				Assert.assertEquals(operand, "stack: 1 ");
				
				return null;
			}
		}).when(consoleManager).write(Mockito.<String>any());
		
		stackManager.write();
	}

	@Test
	public final void testWriteLargeScale() throws CalculatorException {
		stackManager.push(new NumericOperand(new BigDecimal ("1.00000000001")));
		
		Mockito.doAnswer (new Answer<Object>() {

			public Object answer(InvocationOnMock invocation) throws Throwable {
				Assert.assertNotNull(invocation.getArguments()[0]);
				
				Assert.assertTrue(invocation.getArguments()[0] instanceof String);
				
				String operand = (String)invocation.getArguments()[0];
				
				Assert.assertEquals(operand, "stack: 1.0000000000 ");
				
				return null;
			}
		}).when(consoleManager).write(Mockito.<String>any());
		
		stackManager.write();
	}

	@Test (expected = CalculatorException.class)
	public final void testClear() throws CalculatorException {
		stackManager.push(new NumericOperand(new BigDecimal("12.34")));
		
		stackManager.handle(new Clear());
		
		stackManager.pop();
		
		Mockito.verify(stackManager).push(Mockito.<Operand>any());
	}

}
