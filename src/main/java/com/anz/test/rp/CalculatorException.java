package com.anz.test.rp;

public class CalculatorException extends Exception {

	private static final long serialVersionUID = 8376434508872864890L;

	public CalculatorException() {
	}

	public CalculatorException(String message) {
		super(message);
	}

	public CalculatorException(Throwable cause) {
		super(cause);
	}

	public CalculatorException(String message, Throwable cause) {
		super(message, cause);
	}

}
