package com.anz.test.rp.element;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;

import com.anz.test.rp.Calculator;
import com.anz.test.rp.CalculatorException;
import com.anz.test.rp.manager.ElementHandler;
import com.anz.test.rp.manager.StackManager;

public class Subtract implements Operator {

	public static final String SUBTRACT = "-";

	private ArrayList<Operator> undo = new ArrayList<Operator> ();

	public Iterator<Operator> undo() {
		return undo.iterator();
	}

	public boolean execute(ElementHandler elementHandler) throws CalculatorException {
		if (elementHandler != null && elementHandler instanceof StackManager) {
			Operand operator1 = ((StackManager) elementHandler).pop();
			Operand operator2 = ((StackManager) elementHandler).pop();
			
			BigDecimal result = operator2.getOperand().subtract(operator1.getOperand(), Calculator.STORAGE_CONTEXT);
			
			((StackManager) elementHandler).push(new NumericOperand(result));
			
			undo.add(new Pop ());
			undo.add(new Push(operator2));
			undo.add(new Push(operator1));
			
			return true;
		}
		
		return false;
	}

	public Element create(String line) {
		if (SUBTRACT.equals(line)) {
			return new Subtract();
		}
		
		return null;
	}

}
