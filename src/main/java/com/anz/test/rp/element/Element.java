package com.anz.test.rp.element;

import com.anz.test.rp.CalculatorException;


public interface Element {

	Element create (String line) throws CalculatorException;

}
