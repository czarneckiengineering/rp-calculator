package com.anz.test.rp.element;

import java.math.BigDecimal;

import com.anz.test.rp.CalculatorException;

public interface Operand extends Element {
	
	BigDecimal getOperand () throws CalculatorException;
	
}
