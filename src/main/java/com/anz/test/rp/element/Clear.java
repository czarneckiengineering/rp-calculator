package com.anz.test.rp.element;

import java.util.ArrayList;
import java.util.Iterator;

import com.anz.test.rp.manager.ElementHandler;
import com.anz.test.rp.manager.StackManager;

public class Clear implements Operator {

	public static final String CLEAR = "clear";

	private ArrayList<Operator> undo = new ArrayList<Operator> ();

	public Iterator<Operator> undo() {
		return undo.iterator();
	}

	public boolean execute(ElementHandler elementHandler) {
		if (elementHandler != null && elementHandler instanceof StackManager) {
			((StackManager)elementHandler).clear();
			
			return true;
		}
		
		return false;
	}

	public Element create(String line) {
		if (CLEAR.equals(line)) {
			return new Clear();
		}
		
		return null;
	}

}
