package com.anz.test.rp.element;

import java.math.BigDecimal;

public class NumericOperand implements Operand {

	private BigDecimal operand;
	
	public NumericOperand() {
	}

	public NumericOperand(BigDecimal operand) {
		this.operand = operand;
	}

	public Element create (String line) {
		try {
			BigDecimal operand = new BigDecimal (line);
			return new NumericOperand (operand);
		}
		catch (NumberFormatException e) {
			return null;
		}
	}
	
	public BigDecimal getOperand () {
		return operand;
	}

}
