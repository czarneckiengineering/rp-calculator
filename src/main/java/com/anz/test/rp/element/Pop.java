package com.anz.test.rp.element;

import java.util.ArrayList;
import java.util.Iterator;

import com.anz.test.rp.CalculatorException;
import com.anz.test.rp.manager.ElementHandler;
import com.anz.test.rp.manager.StackManager;

public class Pop implements Operator {

	private Operand operand;

	public Pop() {
		super();
	}

	private ArrayList<Operator> undo = new ArrayList<Operator> ();

	public Iterator<Operator> undo() {
		return undo.iterator();
	}

	public boolean execute(ElementHandler elementHandler) throws CalculatorException {
		if (elementHandler != null && elementHandler instanceof StackManager) {
			operand = ((StackManager) elementHandler).pop();
			
			undo.add(new Push(operand));
			
			return true;
		}
		
		return false;
	}

	public Operand getOperand() {
		return operand;
	}

	public Element create(String line) {
		return null;
	}

}
