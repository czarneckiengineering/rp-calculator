package com.anz.test.rp.element;

import java.util.Iterator;

import com.anz.test.rp.CalculatorException;
import com.anz.test.rp.manager.ElementHandler;

public interface Operator extends Element {

	boolean execute(ElementHandler elementHandler) throws CalculatorException;

	Iterator<Operator> undo() throws CalculatorException;

}
