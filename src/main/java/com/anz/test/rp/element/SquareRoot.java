package com.anz.test.rp.element;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;

import com.anz.test.rp.CalculatorException;
import com.anz.test.rp.manager.ElementHandler;
import com.anz.test.rp.manager.StackManager;

public class SquareRoot implements Operator {

	public static final String SQUARE_ROOT = "sqrt";

	private ArrayList<Operator> undo = new ArrayList<Operator> ();

	public Iterator<Operator> undo() {
		return undo.iterator();
	}

	public boolean execute(ElementHandler elementHandler) throws CalculatorException {
		try {
			if (elementHandler != null && elementHandler instanceof StackManager) {
				Operand operator1 = ((StackManager) elementHandler).pop();

				BigDecimal result = sqrt (operator1.getOperand());
				
				((StackManager) elementHandler).push(new NumericOperand(result));
				
				undo.add(new Pop ());
				undo.add(new Push(operator1));
				
				return true;
			}
		}
		catch (NumberFormatException e) {
			throw new CalculatorException (e.getMessage(), e);
		}
		
		return false;
	}

	public Element create(String line) {
		if (SQUARE_ROOT.equals(line)) {
			return new SquareRoot();
		}
		
		return null;
	}

	/**
	 * Method copied from
	 * <a href="http://stackoverflow.com/questions/13649703/square-root-of-bigdecimal-in-java">Square root of BigDecimal in Java</a>}
	 * 
	 * @param value
	 * @return
	 */
	private BigDecimal sqrt(BigDecimal value) {
	    BigDecimal x = new BigDecimal(Math.sqrt(value.doubleValue()));
	    return x.add(new BigDecimal(value.subtract(x.multiply(x)).doubleValue() / (x.doubleValue() * 2.0)));
	}}
