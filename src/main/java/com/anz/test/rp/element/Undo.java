package com.anz.test.rp.element;

import java.util.ArrayList;
import java.util.Iterator;

import com.anz.test.rp.CalculatorException;
import com.anz.test.rp.manager.CommandManager;
import com.anz.test.rp.manager.ElementHandler;

public class Undo implements Operator {
	
	public static final String UNDO = "undo";
	
	private ArrayList<Operator> undo = new ArrayList<Operator> ();

	public Iterator<Operator> undo() {
		return undo.iterator();
	}

	public boolean execute(ElementHandler elementHandler) throws CalculatorException {
		if (elementHandler != null && elementHandler instanceof CommandManager) {
			return ((CommandManager)elementHandler).undo();
		}
		
		return false;
	}

	public Element create(String line) {
		if (UNDO.equals(line)) {
			return new Undo();
		}
		
		return null;
	}

}
