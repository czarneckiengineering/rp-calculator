package com.anz.test.rp.element;

import java.util.ArrayList;
import java.util.Iterator;

import com.anz.test.rp.manager.ElementHandler;
import com.anz.test.rp.manager.StackManager;

public class Push implements Operator {

	private Operand operand;

	public Push(Operand operand) {
		this.operand = operand;
	}

	private ArrayList<Operator> undo = new ArrayList<Operator> ();

	public Iterator<Operator> undo() {
		return undo.iterator();
	}

	public boolean execute(ElementHandler elementHandler) {
		if (elementHandler != null && elementHandler instanceof StackManager) {
			((StackManager) elementHandler).push(operand);
			
			undo.add(new Pop());
			
			return true;
		}
		
		return false;
	}

	public Element create(String line) {
		return null;
	}

}
