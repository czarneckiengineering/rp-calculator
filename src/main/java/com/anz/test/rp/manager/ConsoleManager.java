package com.anz.test.rp.manager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.NoSuchElementException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.anz.test.rp.CalculatorException;
import com.anz.test.rp.Messages;

public class ConsoleManager {

    private static final Logger LOG = LoggerFactory.getLogger(ConsoleManager.class);
    
	private CommandManager commandManager;
	
	private StackManager stackManager;

	private BufferedReader reader;
	
	private PrintStream out;
	
	public ConsoleManager() {
	}

	public void setStackManager(StackManager stackManager) {
		this.stackManager = stackManager;
	}
	
	public void setCommandManager(CommandManager commandManager) {
		this.commandManager = commandManager;
	}
	
	public void setReader(BufferedReader reader) {
		this.reader = reader;
	}

	public void setOut(PrintStream out) {
		this.out = out;
	}

	public void handle() throws CalculatorException {
		LOG.debug("Processing input...");
		
		Pattern pattern = Pattern.compile(Messages.getString("ConsoleManager.0")); //$NON-NLS-1$
		
		try {
			out.print(Messages.getString("ConsoleManager.1")); //$NON-NLS-1$
			
			for (String line = reader.readLine(); line != null && ! "".equals(line); line = reader.readLine()) { //$NON-NLS-1$
				
				Matcher matcher = pattern.matcher(line);
				
				while (matcher.find()) {
					try {
						commandManager.parse(matcher.group());
					}
					catch (CalculatorException e) {
						if (e.getCause() instanceof NoSuchElementException) {
							write ("operator " + matcher.group() + " (position: " + matcher.start() + ") : insufficient parameters"); //$NON-NLS-1$
							break;
						} 
						else {
							write ("operator " + matcher.group() + " (position: " + matcher.start() + ") : " + e.getMessage()); //$NON-NLS-1$
						}
					}
				}

				stackManager.write();

				out.print(Messages.getString("ConsoleManager.1")); //$NON-NLS-1$
			}
		} 
		catch (IOException e) {
			throw new CalculatorException (e.getMessage(), e);
		}
		finally {
			LOG.debug("Finished processing input...");
			try {
				if (reader != null) {
					reader.close();
				}
			} 
			catch (IOException e) {
				throw new CalculatorException (e.getMessage(), e);
			}
		}
	}
	
	public void write (String line) {
		out.println(line);
	}

}
