package com.anz.test.rp.manager;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.anz.test.rp.CalculatorException;
import com.anz.test.rp.Messages;
import com.anz.test.rp.element.Element;
import com.anz.test.rp.element.Operand;
import com.anz.test.rp.element.Operator;
import com.anz.test.rp.element.Push;

public class CommandManager implements ElementHandler {

    private static final Logger LOG = LoggerFactory.getLogger(CommandManager.class);
    
	private ConsoleManager consoleManager;
	
	private StackManager stackManager;
	
	private List<Element> prototypes = new ArrayList<Element> ();

	private Deque<Operator> history = new ArrayDeque<Operator> ();
	
	public CommandManager() {
	}

	public boolean handle(Operator element) throws CalculatorException {
		LOG.debug("Received " + element);
		return element.execute(this);
	}

	public void parse(String line) throws CalculatorException {
		LOG.debug("Parsing " + line);
		Element element = null;
		Operator operator = null;
		
		for (Element prototype : prototypes) {
			element = prototype.create(line);	
			LOG.debug("Created " + element);
			
			if (element != null) {
				if (element instanceof Operand) {
					operator = new Push((Operand)element);
				}
				
				if (element instanceof Operator) {
					operator = (Operator)element;
				}
				
			}
		}
		
		if (operator != null) {
			
			if (!handle(operator)) {
				stackManager.handle(operator);
				history.push(operator);
			}
		}
		
		else {
			consoleManager.write(Messages.getString("CommandManager.0")); //$NON-NLS-1$
		}
		
	}

	public void setConsoleManager(ConsoleManager consoleManager) {
		this.consoleManager = consoleManager;
	}

	public void setStackManager(StackManager stackManager) {
		this.stackManager = stackManager;
	}

	public void addCommand(Element prototype) {
		prototypes.add(prototype);
	}

	public boolean undo() throws CalculatorException {
		Operator last = history.pop();
		
		for (Iterator<Operator> itr = last.undo(); itr.hasNext(); ) {
			Operator operator = itr.next();
			stackManager.handle(operator);
		}
		
		return true;
	}

}
