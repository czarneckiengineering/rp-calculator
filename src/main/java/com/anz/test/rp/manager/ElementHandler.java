package com.anz.test.rp.manager;

import com.anz.test.rp.CalculatorException;
import com.anz.test.rp.element.Operator;

public interface ElementHandler {
	
	boolean handle (Operator element) throws CalculatorException;
	
}
