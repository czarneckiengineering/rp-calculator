package com.anz.test.rp.manager;

import java.math.RoundingMode;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.anz.test.rp.CalculatorException;
import com.anz.test.rp.Messages;
import com.anz.test.rp.element.Operand;
import com.anz.test.rp.element.Operator;

public class StackManager implements ElementHandler {
	
    private static final Logger LOG = LoggerFactory.getLogger(StackManager.class);
    
	private Deque<Operand> stack = new ArrayDeque<Operand> ();
	
	private ConsoleManager consoleManager;

	public void setConsoleManager(ConsoleManager consoleManager) {
		this.consoleManager = consoleManager;
	}

	public StackManager() {
	}
	
	public boolean handle (Operator operator) throws CalculatorException {
		LOG.debug("Received " + operator);
		return operator.execute (this);
	}
	
	public void push (Operand operand) {
		stack.push(operand);
	}
	
	public Operand pop () throws CalculatorException {
		try {
			return stack.pop();
		}
		catch (NoSuchElementException e) {
			throw new CalculatorException(e.getMessage(), e);
		}
	}

	public void write() throws CalculatorException {
		String line = Messages.getString("StackManager.0"); //$NON-NLS-1$
		
		for (Operand operand : stack) {
			if (operand.getOperand().scale() > 10)
				line = operand.getOperand().setScale(10, RoundingMode.HALF_UP).toPlainString() + Messages.getString("StackManager.1") + line; //$NON-NLS-1$
			else
				line = operand.getOperand().toPlainString() + Messages.getString("StackManager.2") + line; //$NON-NLS-1$
		}
		
		consoleManager.write(Messages.getString("StackManager.3") + line); //$NON-NLS-1$
	}

	public void clear() {
		stack.clear();
	}
	
}
