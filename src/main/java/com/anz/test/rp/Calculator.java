package com.anz.test.rp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.MathContext;
import java.math.RoundingMode;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.anz.test.rp.element.Add;
import com.anz.test.rp.element.Clear;
import com.anz.test.rp.element.Divide;
import com.anz.test.rp.element.Multiply;
import com.anz.test.rp.element.NumericOperand;
import com.anz.test.rp.element.SquareRoot;
import com.anz.test.rp.element.Subtract;
import com.anz.test.rp.element.Undo;
import com.anz.test.rp.manager.CommandManager;
import com.anz.test.rp.manager.ConsoleManager;
import com.anz.test.rp.manager.StackManager;

public class Calculator {

	public static final MathContext STORAGE_CONTEXT = new MathContext (15, RoundingMode.HALF_UP);

    private static final Logger LOG = LoggerFactory.getLogger(Calculator.class);

	private ConsoleManager consoleManager;

	private CommandManager commandManager;

	private StackManager stackManager;
    
	public static void main(String[] args) {
		Calculator calculator = new Calculator();
		calculator.consoleManager = new ConsoleManager ();
		calculator.consoleManager.setReader(new BufferedReader(new InputStreamReader(System.in)));
		calculator.consoleManager.setOut(System.out);
		calculator.commandManager = new CommandManager ();
		calculator.stackManager = new StackManager ();
		calculator.handle();
	}
	
	public void handle() {
		
	    LOG.info("Starting calculator");
		
		try {
			consoleManager.setCommandManager (commandManager);
			consoleManager.setStackManager(stackManager);
			
			commandManager.setConsoleManager (consoleManager);
			commandManager.setStackManager(stackManager);
			
			commandManager.addCommand (new Add());
			commandManager.addCommand (new Subtract());
			commandManager.addCommand (new Multiply());
			commandManager.addCommand (new Divide());
			commandManager.addCommand (new SquareRoot());
			commandManager.addCommand (new Undo());
			commandManager.addCommand (new Clear());
			commandManager.addCommand (new NumericOperand());
			
			stackManager.setConsoleManager(consoleManager);
			
			consoleManager.handle();
		}
		catch (CalculatorException e) {
			System.err.println(Messages.getString("Calculator.0"));	 //$NON-NLS-1$
			e.printStackTrace();
		    LOG.error(e.getMessage(), e);
			
		}
		finally {
			System.out.println(Messages.getString("Calculator.1")); //$NON-NLS-1$
		    LOG.info("Exiting calculator");
		}

	}

	public void setConsoleManager(ConsoleManager consoleManager) {
		this.consoleManager = consoleManager;
	}

	public void setCommandManager(CommandManager commandManager) {
		this.commandManager = commandManager;
	}

	public void setStackManager(StackManager stackManager) {
		this.stackManager = stackManager;
	}

}
